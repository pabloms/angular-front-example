import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/shared/services/products.service';

@Component({
  selector: 'app-products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.scss']
})
export class ProductsPageComponent implements OnInit {

  productsStyle: boolean = true;

  products: any[];
  filterArray: any[];

  length: number;
  filterLength: number;

  searchValue: string = "";

  constructor(private productsService: ProductsService) { }

  ngOnInit(): void {
    this.productsService.getProducts().subscribe((res: any) => {
      this.products = res;
      this.filterArray = this.products;
      this.filterLength = this.products.length;
  })
}

  listStyle() {
    this.productsStyle = false;
  }

  cardStyle() {
    this.productsStyle = true;
  }

  filterProducts() {
    this.filterArray = this.products.filter(product => product.name.toLowerCase().includes(this.searchValue.toLowerCase()));
    this.filterLength = this.filterArray.length;
  }

}
