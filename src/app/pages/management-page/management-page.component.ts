import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ProductsService } from 'src/app/shared/services/products.service';

@Component({
  selector: 'app-management-page',
  templateUrl: './management-page.component.html',
  styleUrls: ['./management-page.component.scss']
})
export class ManagementPageComponent implements OnInit {

  productForm;
  productStyle: boolean = true;

  constructor(private formBuilder: FormBuilder, private productsService: ProductsService) {
    this.productForm = this.formBuilder.group({
      name: [''],
      price: [''],
      description: [''],
      image: ['https://image.flaticon.com/icons/png/512/41/41943.png'],
      stars: [''],
    })
}

postProduct() {
  console.log(this.productForm.value)
  return this.productsService.postProducts(this.productForm.value).subscribe();
}

  ngOnInit(): void {
  }

}
